package com.itau.bingoapi.controllers;

import java.util.HashMap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.bingoapi.models.Cartela;
import com.itau.bingoapi.models.SaidaCartela;
import com.itau.bingoapi.models.Sorteio;

@RestController
public class Controller {

	@RequestMapping("/bingo/cartela")
	public SaidaCartela getCartela() {
		return new SaidaCartela(new Cartela());
	}
	
	@RequestMapping("/bingo/sorteio")
	public ResponseEntity<?> getSorteio() {
		int sorteado = Sorteio.getSorteado();
		if (sorteado == 0) {
			HashMap<String,String> saida = new HashMap<>();
			saida.put("erro", "Sorteio finalizado");
			return ResponseEntity.status(400).body(saida);
		} else {
			HashMap<String,Integer> saida = new HashMap<>();
			saida.put("numero", sorteado);
			return ResponseEntity.status(200).body(saida);
		}
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/bingo/reiniciar")
	public void reiniciarSorteio() {
		Sorteio.resetar();
	}
	
}
