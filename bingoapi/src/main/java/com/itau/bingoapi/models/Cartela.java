package com.itau.bingoapi.models;

import java.util.ArrayList;

public class Cartela {

	private int[][] numerosCartela = new int[4][4];
	
	public Cartela() {
		ArrayList<Integer> numeros = new ArrayList<>();
		for (int i = 1; i <= 60; i++) {
			numeros.add(i);
		}
		for (int i = 0; i < 4 ; i++) {
			for (int j=0; j < 4; j++) {
				int index =  (int)Math.ceil(Math.random()*numeros.size()-1);
				numerosCartela[i][j] = numeros.get(index);
				numeros.remove(index);
			}
		}
	}
	
	public int [][] getMatriz() {
		return numerosCartela;
	}
}
