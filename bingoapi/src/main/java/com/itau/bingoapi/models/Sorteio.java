package com.itau.bingoapi.models;

import java.util.ArrayList;

public class Sorteio {
	
	private static boolean ativo = false;
	private static ArrayList<Integer> numerosSorteio = new ArrayList<>();
	
	public Sorteio() {
		resetar();
	}
	
	public static int getSorteado() {
		if (!ativo) {
			resetar();
			ativo = true;
		}
		if (numerosSorteio.size() == 0) {
			return 0;
		} else {
			int index = (int)Math.ceil(Math.random()*numerosSorteio.size()-1);
			int numeroSorteado = numerosSorteio.get(index);
			numerosSorteio.remove(index); 
			return numeroSorteado;
		}
	}

	public static void resetar() {
		numerosSorteio.clear();
		for (int i = 1; i <= 60; i++) {
			numerosSorteio.add(i);
		}
	}
	
}
