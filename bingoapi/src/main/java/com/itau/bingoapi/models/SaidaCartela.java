package com.itau.bingoapi.models;

public class SaidaCartela {

	public String titulo = new String("Bingo");
	public int[][] conteudo = new int [4][4];
	
	public SaidaCartela(Cartela cartela) {
		conteudo = cartela.getMatriz();
	}
}
